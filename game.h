#ifndef GAME_H
#define GAME_H
#include <QGraphicsScene>
#include <QGraphicsView>
#include "myrect.h"
#include <QTimer>
#include "enemy.h"
#include <QObject>
#include <QWidget>
#include "score.h"
#include "health.h"

class Game: public QGraphicsView
{
public:
    Game(QWidget * parent=0);

    QGraphicsScene *scene;
    MyRect * player;
    Score * score;
    Health * health;

};

#endif // GAME_H
