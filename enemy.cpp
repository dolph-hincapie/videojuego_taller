#include "Enemy.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QDebug>
#include "game.h"

extern Game * game;

Enemy::Enemy()
{
    int random_number = rand() % 700;
    setPos(random_number,0);

    setPixmap(QPixmap(":/images/enemy"));

    //Conexion
    QTimer * timer = new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    timer->start(50);
}

void Enemy::move()
{
    setPos(x(),y()+5);
    if(pos().y() > 500){
        game->health->decrease();
        scene()->removeItem(this);
        delete this;
    }
}













